#TurtlTeck Python Client
#V0.1 Alpha
#Last Updated 15th of Feb 2015
import urllib
import urllib2
from pprint import pprint
class robot:
	"""TurtlTeck Python Client for the new Pi Based version"""
	#Begin Configuration
	#Default Robot ID, Set this to the Robot ID either displayed on your robot or in your robot config file.

	#Default Secret, This is the passphrase automagically generated in your account. This passphrase is the same for all robots on your account.

	
	
	#Server URL - Shouldn't need to be changed.
	rtkServer = "http://dev.ryanteck.uk/turtlTeckPi/"
	#API Url - Shouldn't need to be changed.
	pythonApi = "api/pythonSubmit.php"
	#Authentication Check - Shouldn't need to be changed.
	pythonAuth = "api/pythonAuth.php"
	
	
	#From here on down shouldn't need to be modified at all for configuration.
	def __init__(self,robotId,secretWord,turtleEnable):
		if(turtleEnable): 
			#User wants to use python turtle
			import turtle
			self.turtleScreen = turtle.Turtle()
			self.turtleScreen.speed(1)
		
		self.turtleEnable = turtleEnable
		
		self.allSteps = []
		self.robotId = robotId
		self.secretWord = secretWord
		self.isSetup = 1
		#Connect to the server
		#First we should check if the robotId and secretWord is valid
		self.checkAuth()
		
	

	def forward(self,steps):
		if(self.isSetup == 0):
			print("Warning you have not setup your code")
		
		#add forward x steps to the steps array
		self.allSteps.append("fd"+str(steps))
		#Finally Pyturtle
		if(self.turtleEnable): self.turtleScreen.forward(steps)
		
	def backward(self,steps):
		if(self.isSetup == 0):
			print("Warning you have not setup your code")
		#add forward x steps to the steps array
		self.allSteps.append("bk"+str(steps)) 	
		if(self.turtleEnable): self.turtleScreen.backward(steps)
 
	def left(self,steps):
		if(self.isSetup == 0):
			print("Warning you have not setup your code")
		#add right x degrees to the steps array
		self.allSteps.append("lt"+str(steps)) 
		if(self.turtleEnable): self.turtleScreen.left(steps)
		
	def right(self,steps):
		if(self.isSetup == 0):
			print("Warning you have not setup your code")
		#add left x degrees to the steps array
		self.allSteps.append("rt"+str(steps)) 
		if(self.turtleEnable): self.turtleScreen.right(steps)
		
	def send(self):
		print str(self.allSteps)
		convertedSteps = ""
		for eachStep in self.allSteps:
			
			convertedSteps = convertedSteps+(eachStep)+","
		stepsLength = len(convertedSteps)-1
		convertedSteps = convertedSteps[:stepsLength] #Fix last , easily
			
		
		pprint(convertedSteps)
		data = {"robotID":self.robotId, 'secretWord' : self.secretWord,"steps":convertedSteps}
		dataEncoded = urllib.urlencode(data)
		urlstring = self.rtkServer+self.pythonApi
		print urlstring
		dataUrl = urllib.urlopen(urlstring,dataEncoded)
		result = (dataUrl.read())
		pprint (str(result))
		print "Data sent to server, press Enter / Return to continue"
		raw_input()
	
	def checkAuth(self):
		print("Testing Robot Configuration Details")
		data = {"robotId":self.robotId, 'secretWord' : self.secretWord}
		dataEncoded = urllib.urlencode(data)
		urlstring = (str(self.rtkServer+self.pythonAuth+"?"+dataEncoded))
		print urlstring
		dataUrl = urllib.urlopen(urlstring)
		result = (dataUrl.read())
		resultInt = 0
		try:
			resultInt = int(result)
		except ValueError:
			print "Error with server"
			exit()
			return False
			
		if(resultInt != 1):
			print("Robot ID or Secret Key is incorrect")
			exit()
			return False
		

